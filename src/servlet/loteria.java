package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class loteria
 */
@WebServlet("/loteria")
public class loteria extends HttpServlet {
	private static final long serialVersionUID = 1L;
	ArrayList<Integer> apuesta = new ArrayList<Integer>();
	String mensaje;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public loteria() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getParameter("method") != null && request.getParameter("method").equals("apostar")){
			apostar(request,response);
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/loteria.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getParameter("method") != null && request.getParameter("method").equals("realizar")){
			realizar(request,response);
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/loteria.jsp");
		dispatcher.forward(request, response);
	}
	
	protected void apostar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		Integer numero = Integer.parseInt(request.getParameter("Numero"));
		for(int n : apuesta){
			if(n == numero){
				apuesta.remove(numero);
				request.setAttribute("apuesta", apuesta);
				return;
			}
		}
		apuesta.add(numero);
		request.setAttribute("apuesta", apuesta);
	}
	
	protected void realizar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		
		if(apuesta.size() < 6){
            mensaje = "La apuesta minima es de 6 y tu tienes " + apuesta.size();
        }else if (apuesta.size() == 6) {
            mensaje = "Apuesta simple";
        }else{
            mensaje = "Apuesta multiple: " + apuesta.size();
        }
		request.setAttribute("mensaje", mensaje);
		request.setAttribute("apuesta", apuesta);
	}

}
