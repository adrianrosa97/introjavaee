package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Calculadora
 */
@WebServlet("/calculadora")
public class Calculadora extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Calculadora() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String user = (String)session.getAttribute("user");
		
		if(user == null){
			response.sendRedirect(request.getContextPath() + "/login");
		}else{
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/calculadora.jsp");
			dispatcher.forward(request, response);
		}
		return;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int operador1;
		int operador2;
		int resultado;
		try{
			operador1 = Integer.parseInt(request.getParameter("operador1"));
		}catch (NumberFormatException e) {
			operador1 = 0;
		}
		try{
			operador2 = Integer.parseInt(request.getParameter("operador2"));
		}catch (NumberFormatException e) {
			operador2 = 0;
		}
		if(request.getParameter("operacion").equals("sumar")){
			resultado = operador1 + operador2;
		}else if(request.getParameter("operacion").equals("restar")){
			resultado = operador1 - operador2;
		}else if(request.getParameter("operacion").equals("multiplicar")){
			resultado = operador1 * operador2;
		}else{
			resultado = operador1 / operador2;
		}
//		HttpSession session = request.getSession();
//		session.setAttribute("resultado", resultado);
		request.setAttribute("resultado", resultado);
		request.setAttribute("operador1", operador1);
		request.setAttribute("operador2", operador2);
		request.setAttribute("operacion", request.getParameter("operacion"));
		
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/calculadora.jsp");
		dispatcher.forward(request, response);
	}

}
