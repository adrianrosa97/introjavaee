<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Calculadora</title>
</head>
<body>
	<h1>Calculadora con Java EE</h1>
	<hr>
	<h3><%= request.getSession().getAttribute("user") %>
	<form method="post" action="/introjavaee/login?method=logOut">
		<input type="submit" value="Cerrar Sesion">
	</form>
	</h3>
	<form action="" method="post">
		<label>Operador 1</label> <input type="text" name="operador1" value="<%= request.getAttribute("operador1")%>"><br>
		<select name="operacion">
			<option value="sumar" <%= request.getAttribute("operacion") != null && request.getAttribute("operacion").equals("sumar") ? "SELECTED" : ""%> >Sumar</option>
			<option value="restar" <%= request.getAttribute("operacion") != null && request.getAttribute("operacion").equals("restar") ? "SELECTED" : ""%>>Restar</option>
			<option value="multiplicar" <%= request.getAttribute("operacion") != null && request.getAttribute("operacion").equals("multiplicar") ? "SELECTED" : ""%>>Multiplicar</option>
			<option value="dividir" <%= request.getAttribute("operacion") != null && request.getAttribute("operacion").equals("dividir") ? "SELECTED" : ""%>>Dividir</option>
		</select><br> 
		<label>Operador 2</label> <input type="text" name="operador2" value="<%= request.getAttribute("operador2")%>"><br>
		<hr>
		<input type="submit" value="Operar">
	</form>
	<br>
	<%if(request.getAttribute("resultado") != null){ %>
	<h4>Resultado: <%= request.getAttribute("resultado") %></h4>
	<%} %>
</body>
</html>