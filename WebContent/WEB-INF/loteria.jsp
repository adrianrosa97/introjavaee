<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Loteria</title>
</head>
<body>
	<h1 align="center">Loteria</h1>
    <form action="/introjavaee/loteria?method=realizar" method="post">
    <table class="tableClass" border="1" align="center">
        <% for (Integer i=1; i <= 49; i++) {
        	if(request.getAttribute("apuesta") != null){
        		ArrayList<Integer> apuesta = (ArrayList<Integer>) request.getAttribute("apuesta");
                out.print("<td style='background-color: " + (apuesta.contains(i) ? "green" : "") + " '><a href='/introjavaee/loteria?method=apostar&Numero=" + i + "'>" + i + "</a></td>");
        	}else{
        		out.print("<td><a href='/introjavaee/loteria?method=apostar&Numero=" + i + "'>" + i + "</a></td>");
        	}
        	if (i % 7 == 0) {
                out.print("</tr><tr>");
            }

        } %>
    </table>
	<h3 align="center"><%= request.getAttribute("mensaje") != null ? request.getAttribute("mensaje") : ""  %></h3>
    <div align="center" style="background-color: green">
        <input type="submit" name="Enviar" value="Apostar">
    </div>

    </form>
    <hr>
    <div class="apuestas" align="center">
        <% if (request.getAttribute("apuesta") != null) {%>
            <% for (Integer numero : (ArrayList<Integer>) request.getAttribute("apuesta")){ %>
                Has apostado al numero: <%= numero %><br>
            <% } %>
        <% } %>
    </div>
</body>
</html>